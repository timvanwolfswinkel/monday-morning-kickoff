import Vue from 'vue';
import Vuex from 'vuex';

import fetchContent from './api/contentful';

Vue.use(Vuex);

const SET_LOADING = 'SET_LOADING';
const LIST_OF_MEMBERS = 'LIST_OF_MEMBERS';

export default new Vuex.Store({
  state: {
    content: [],
    loading: false,
  },
  mutations: {
    [SET_LOADING](state, loading) {
      state.loading = loading;
    },
    [LIST_OF_MEMBERS](state, members) {
      console.log(members);
      state.content = members;
    },
  },
  actions: {
    getContent({ commit }) {
      commit(SET_LOADING, true);
      fetchContent().then((response) => {
        commit(LIST_OF_MEMBERS, response.data.items);
        commit(SET_LOADING, false);
      });
    },
  },
});
