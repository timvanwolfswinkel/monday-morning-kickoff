import axios from 'axios';

const API_URL = 'https://cdn.contentful.com';
const SPACE_ID = '000';
const ACCESS_TOKEN = '000';

/**
 * Fetch content from Contentful
 * @return {Promise<Object>} List of festivals
 * @see {@link https://cdn.contentful.com}
 */
export default function fetchContent() {
  return axios.get(`${API_URL}/spaces/${SPACE_ID}/entries?access_token=${ACCESS_TOKEN}`);
}
